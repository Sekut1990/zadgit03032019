import java.util.Arrays;

public class Zadanie3 {

    int [] tab1 = new int[9]; //tworze tablice z 9 elementami

   public void metoda3a(){ //tworze metode za pomoca ktorej wyprintuje wszystkie elementy tablicy po kolei

        for (int i=0; i< tab1.length; i++) {
            tab1[i] = i +1 ;
        }
        System.out.println(Arrays.toString(tab1));

    }

  public void metoda3b(){ // metoda za pomoca ktorej wyprintuje wszystkie elementy od tylu

        for (int i = tab1.length-1; i>=0; i-- ){
            tab1[i]= tab1.length-i;
        }
            System.out.println(Arrays.toString(tab1));
    }



    public void metoda3c(){ // za pomoca if(modulo) szukam w tablicy liczb nieparzystych, nastepnie kaze je wyprintowac
        System.out.println("Liczby nieparzyste w tablicy to: ");
        for (int i=0; i< tab1.length; i++) {//doustny test tolerancji glukozy
            if (tab1[i]% 2 ==1) {
                System.out.println(tab1[i]);
            }
        }
    }

   public void metoda3d(){
       System.out.println("Liczby w tablicy podzielne przez 3 to:");
       for (int i=0; i< tab1.length; i++) {
            if (tab1[i]% 3 ==0) {//jezeli liczby z tab, po podzieleniu przez trzy nie beda mialy reszty, printowac
                System.out.println(tab1[i]);
            }
        }
    }

    public void metoda3e(){
       int suma= 0; // zmienna dodatkowa, ktora bedzie pozwalala sumowac elemnty tablicy
       for (int i=0; i< tab1.length; i++){
           suma+= tab1[i]; //kod sumujacy sumujacy liczby w tablicy
       }
        System.out.println("Suma tablicy to: " + suma);
    }




}
