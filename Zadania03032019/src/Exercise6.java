import java.util.Scanner;

import static java.lang.Math.pow;

public class Exercise6 {

    double rates1 = 0.08;
    double rates2 = 0.13;
    double rates3 = 0.16;

    /* method retrieves data from the user: amount we want to invest (sum), how long the investment will last (year).
    I set apart three types of annual interest rates, which depend on the number of years entered. With the "if" method
    I define the terms for each of the three data ranges*/

    public void method6 (){

        Scanner scanner6 = new Scanner(System.in);

        System.out.println("Podaj kwote jaka chcesz wplacic: ");
        int sum = scanner6.nextInt();

        System.out.println("Przez ile lat chcialbys, aby inwestycja trwala?");
        int year = scanner6.nextInt();

        double resoult1 = pow((1.00 + rates1), year);
        double resoult2 = pow((1.00 + rates2), year);
        double resoult3 = pow((1.00 + rates3), year);


        if (year < 5){
            System.out.println("Twoje oprocentowanie wynosi " + rates1 + " w skali roku.");
            System.out.println("Twoja stopa zwrotu po " + year + " latach wynosi: " + ((resoult1-1) * 1.00));
            System.out.println("Przy wplaceniu " + sum + " zl uzyskasz po " + year + " latach: " + ((((resoult1-1) * 1.00)*sum)+sum) + "zl");
        }else if (year >5 && year< 10){
            System.out.println("Twoje oprocentowanie wynosi " + rates2 + " w skali roku.");
            System.out.println("Twoja stopa zwrotu po " + year + " latach wynosi: " + ((resoult2-1) * 1.00));
            System.out.println("Przy wplaceniu " + sum + " zl uzyskasz po " + year + " latach: " + ((((resoult2-1) * 1.00)*sum)+sum) + "zl");
        } else {
            System.out.println("Twoje oprocentowanie wynosi " + rates3 + " w skali roku.");
            System.out.println("Twoja stopa zwrotu po " + year + " latach wynosi: " + ((resoult3-1) * 1.00));
            System.out.println("Przy wplaceniu " + sum + " zl uzyskasz po " + year + " latach: " + ((((resoult3-1) * 1.00)*sum)+sum) + "zl");
        }




    }
}
