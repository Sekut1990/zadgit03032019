import java.util.Scanner;

public class Zadanie5 {

    public void metoda5(){
        Scanner scanner5 = new Scanner(System.in);

        System.out.println("Podaj trzy liczby calkowite: ");
        int num1= scanner5.nextInt();
        int num2= scanner5.nextInt();
        int num3= scanner5.nextInt();

        int suma= num1+ num2+ num3;
        double srednia= (num1+ num2+ num3)/ 3.00;

        System.out.println("Suma podanych liczb to: " + suma);
        System.out.println("Srednia podanych liczb to: " + srednia);

    }
}
