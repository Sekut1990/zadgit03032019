import java.util.Scanner;

public class Exercise7 {

    /*Method retrieves data from the user a year. Then it meets the condition in which the year is leap if it is
    divisible 4, but it is not divisible by 100 unless it is divisible by 400.*/

    public void method7(){
        Scanner scanner7= new Scanner(System.in);

        System.out.println("Podaj jakis rok: ");
        int year= scanner7.nextInt();

        if (year%4==0 && year%100!=0 || year%400==0){
            System.out.println("Rok jest przestepny :-)");
        }else {
            System.out.println("Rok nie jest przestepny :-(");
        }
    }
}
