import java.util.*;

public class WarmupPlus1 {

    /*Method takes 4 numbers from the user, then I place the numbers in the list and display them in reverse order*/

    public void method8(){

        Scanner scanner8= new Scanner(System.in);

        System.out.println("Podaj ciag 4 liczb calkowitych: ");
        int num1= scanner8.nextInt();
        int num2= scanner8.nextInt();
        int num3= scanner8.nextInt();
        int num4= scanner8.nextInt();

        List<Integer> list8= new ArrayList<>();
        list8.add(num1);
        list8.add(num2);
        list8.add(num3);
        list8.add(num4);

        Collections.reverse(list8);
        for(Integer i: list8){

            System.out.println(i);
        }

    }
}
